def FAILED_STAGE

pipeline {
    agent any
    environment {
        DOCKERHUB_CREDENTIALS = credentials('dockerhub')
    }
    stages {
        stage('Checkout') {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                    echo "Stage: Checkout"
                }

                echo "Pulling from gitlab repository ..."

                checkout([$class: 'GitSCM', branches: [[name: '*/main']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab_username', url: 'https://gitlab.com/mahou.marouen/angular-test.git']]])

                echo "Pulling from gitlab repository finished!"
            }
        }
        stage(build) {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                    echo "Stage: Build"
                }
                echo 'Installing dependencies ...'
                updateGitlabCommitStatus name: 'build', state: 'running'
                bat 'npm install'
                echo 'Dependencies installed!'
                echo 'Building angular app ...'
                bat 'docker build -t marouenmahou/angular-app:latest .'
                echo 'Building angular app completed !'
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }    

        stage(test) {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                    echo "Stage: Test"
                }
                updateGitlabCommitStatus name: 'test', state: 'running'
                echo 'testing angular app ...'
                bat 'npm run test-karma'
                echo 'testing angular app completed !'
                echo 'publishing test reports to sonarQube ...'
                bat 'npm run sonar'
                echo 'test reports published successfuly !'
                updateGitlabCommitStatus name: 'test', state: 'success' 
            }
        }

        stage(release) {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                    echo "Stage: Release"
                }

                updateGitlabCommitStatus name: 'release', state: 'running'
                echo 'Connecting to dockerhub ...'
                bat "docker login -u $DOCKERHUB_CREDENTIALS_USR -p $DOCKERHUB_CREDENTIALS_PSW"
                echo 'Connected to dockerhub successfuly !'

                echo 'pushing image to dockerhub ...'
                bat "docker push marouenmahou/angular-app:latest"
                echo 'image pushed to dockerhub successfuly !'
                updateGitlabCommitStatus name: 'release', state: 'success'
            }
        }
    }
    post {
        failure {
            echo "Failed stage name: ${FAILED_STAGE}"
            updateGitlabCommitStatus name: "${FAILED_STAGE}", state: 'failed'
        }
        always {
            echo 'Logging out from dockerhub...'
            bat 'docker logout'
            echo 'Logged out from dockerhub'
        }
    }
}
